"""
Store mosaik simulation data in an InfluxDB v2 database.

"""
import collections

import mosaik_api_v3 as  mosaik_api
from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS
import datetime
import arrow


META = {
    'type': 'event-based',
    'models': {
        'Database': {
            'public': True,
            'any_inputs': True,
            'params': ['influx_url', 'influx_token', 'influx_org', 'influx_bucket'],
            'attrs': [],
        },
    },
}

ENTITY_ID = 'Database'


class InfluxDBCollector(mosaik_api.Simulator):
    def __init__(self):
        super().__init__(META)
        self.data = collections.defaultdict(lambda:
                                            collections.defaultdict(list))
        self.step_size = None

        self.influx_connection = None
        self.influx_bucket = None
        self.time_resolution = 1.0
        self.sim_start = None
        self.simulation_run_id = None
        self.created = False

    def init(self, sid, time_resolution=1.0, sim_start=0, date_format=None, simulation_run_id=str(datetime.datetime.now())):
        self.sim_start = self.get_time(sim_start, date_format)
        self.time_resolution = time_resolution
        self.simulation_run_id = simulation_run_id
        return self.meta

    def create(self, num, model, influx_url, influx_token, influx_org, influx_bucket):
        if num > 1 or self.created:
            raise RuntimeError('Can only create one instance of Monitor.')

        self.influx_connection = InfluxDBClient(url=influx_url, token=influx_token, org=influx_org)
        self.influx_bucket = influx_bucket

        self.created = True
        return [{'eid': ENTITY_ID, 'type': model}]

    def step(self, time, inputs, max_advance):
        data = inputs[ENTITY_ID]

        # get new date and time
        now = self.sim_start.shift(seconds=time * self.time_resolution).datetime

        with self.influx_connection.write_api(write_options=SYNCHRONOUS) as write_api:
            for attr, values in data.items():
                for src, value in values.items():
                    self.data[src][attr].append(value)

                    if len(src.split('.')[0].split('-')) == 2:
                        _simulator = src.split('.')[0]
                        _simulatortype = src.split('-')[0].split('-')[0]
                        _entitytype = ".".join(src.split('-')[1].split('_')[0].split('.')[1:]).split(":")[0]
                        _entity = ".".join(src.split('.')[1:])
                    elif len(src.split('.')[0].split('-')) == 3:
                        _source = src.split('-')[0:2]
                        _destination = src.split('-')[2]
                        _destinationtype = src.split('-')[2].split('_')[0]
                    
                    # execution_time kept for legacy compatibility
                    p = Point("simulation")\
                        .tag("relation", src)\
                        .tag("execution_time", self.simulation_run_id)\
                        .tag("simulation_run_id", self.simulation_run_id)\
                        .tag("simulator", _simulator)\
                        .tag("entity", _entity)\
                        .tag("simulator_type", _simulatortype)\
                        .tag("entity_type", _entitytype)\
                        .field(attr, value)\
                        .time(now)
                    write_api.write(bucket=self.influx_bucket, record=p)

        return None

    def finalize(self):
        pass

    def get_time(self, date, date_format):
        if date_format is not None:
            return arrow.get(date, self.date_format)
        else:
            return arrow.get(date)


def main():
    mosaik_api.start_simulation(InfluxDBCollector())


if __name__ == '__main__':
    mosaik_api.start_simulation(InfluxDBCollector())
